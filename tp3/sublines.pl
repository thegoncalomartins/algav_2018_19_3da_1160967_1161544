:- module(sublines, [element/3, subline_right/4, subline_left/4, subline_above/4, subline_below/4, subline_diag_up_right/4, subline_diag_up_left/4, subline_diag_down_right/4, subline_diag_down_left/4]).


% element(+List, +Position, -Element)
%
%   Retrieves the Element at Position in List.
%
element([Element|_], 1, Element) :- !.

element([_|List], Position, Element) :-
    Position1 is Position - 1,
    element(List, Position1, Element).

% subline_right(+Board, +Position, +N, -List)
%
%   Computes the sublist ( [(Position, Element)|_]) ) to the right of
%   Position. Board has NxN positions. Position starts at 1.
%
%   e.g. Position = 1, [a, b,
%                       c, d] -> [b]
%
subline_right(_, Pos, N, []) :-
    Pos mod N =:= 0,
    !.

subline_right(B, Pos, N, List) :-
    PosToAdd is N - (Pos mod N),
    subline_right_rec(B, Pos, PosToAdd, List).

subline_right_rec(_, _, 0, []) :- !.

subline_right_rec(B, Pos, PosToAdd, [(Pos1, E)|List]) :-
    Pos1 is Pos + 1,
    element(B, Pos1, E),
    PosToAdd1 is PosToAdd - 1,
    subline_right_rec(B, Pos1, PosToAdd1, List).


% subline_left(+Board, +Position, +N, -List)
%
%   Computes the sublist ( [(Position, Element)|_]) ) to the left of Position.
%   Board has NxN positions.
%   Position starts at 1.
%
%   e.g. Position = 2, [a, b,
%                       c, d] -> [a]
%
subline_left(B, Pos, N, List) :-
    Pos mod N =:= 0,
    PosToAdd is N - 1,
    subline_left_rec(B, Pos, PosToAdd, List), !.

subline_left(B, Pos, N, List) :-
    PosToAdd is (Pos mod N) - 1,
    subline_left_rec(B, Pos, PosToAdd, List).

subline_left_rec(_, _, 0, []) :- !.

subline_left_rec(B, Pos, PosToAdd, [(Pos1, E)|List]) :-
    Pos1 is Pos - 1,
    element(B, Pos1, E),
    PosToAdd1 is PosToAdd - 1,
    subline_left_rec(B, Pos1, PosToAdd1, List).


% subline_above(+Board, +Position, +N, -List)
%
%   Computes the sublist ( [(Position, Element)|_]) ) above Position.
%   Board has NxN positions.
%   Position starts at 1.
%
%   e.g. Position = 4, [a, b,
%                       c, d] -> [b]
%
subline_above(B, Pos, N, List) :-
    Pos mod N =:= 0,
    PosToAdd is (Pos div N) - 1,
    subline_above_rec(B, Pos, N, PosToAdd, List), !.

subline_above(B, Pos, N, List) :-
    PosToAdd is (Pos div N),
    subline_above_rec(B, Pos, N, PosToAdd, List), !.

subline_above_rec(_, _, _, 0, []) :- !.

subline_above_rec(B, Pos, N, PosToAdd, [(Pos1, E)|List]) :-
    Pos1 is Pos - N,
    element(B, Pos1, E),
    PosToAdd1 is PosToAdd - 1,
    subline_above_rec(B, Pos1, N, PosToAdd1, List).


% subline_below(+Board, +Position, +N, -List)
%
%   Computes the sublist ( [(Position, Element)|_]) ) above Position.
%   Board has NxN positions.
%   Position starts at 1.
%
%   e.g. Position = 1, [a, b,
%                       c, d] -> [c]
%
% if Pos > N^2, then no sublist below, List is empty ([]).
%
subline_below_rec(_, Pos, N, []) :-
    Pos > N * N, !.

subline_below_rec(B, Pos, N, [(Pos, E)|List]) :-
    element(B, Pos, E),
    Pos1 is Pos + N,
    subline_below_rec(B, Pos1, N, List).

subline_below(B, Pos, N, List) :-
    Pos1 is Pos + N,
    subline_below_rec(B, Pos1, N, List).


% subline_diag_up_right(+Board, +Position, +N, -List)
%
%   Computes the sublist ( [(Position, Element)|_]) ) to the upper right
%   (diagonally) of Position.
%   Board has NxN positions.
%   Position starts at 1.
%
%   e.g. Position = 3, [a, b,
%                       c, d] -> [b]
%
subline_diag_up_right(_, Pos, N, []) :-
    Pos mod N =:= 0, !.

subline_diag_up_right(B, Pos, N, List) :-
    Pos1 is Pos - N + 1,
    subline_diag_up_right_rec(B, Pos1, N, List).

subline_diag_up_right_rec(_, Pos, _, []) :-
    Pos =< 0, !.

subline_diag_up_right_rec(B, Pos, N, [(Pos, E)]) :-
    Pos mod N =:= 0,
    element(B, Pos, E),
    !.

subline_diag_up_right_rec(B, Pos, N, [(Pos, E)|List]) :-
    element(B, Pos, E),
    Pos1 is Pos - N + 1,
    subline_diag_up_right_rec(B, Pos1, N, List).


% subline_diag_up_left(+Board, +Position, +N, -List)
%
%   Computes the sublist ( [(Position, Element)|_]) ) to the upper left
%   (diagonally) of Position.
%   Board has NxN positions.
%   Position starts at 1.
%
%   e.g. Position = 4, [a, b,
%                       c, d] -> [a]
%
subline_diag_up_left(_, Pos, N, []) :-
    (Pos - 1) mod N =:= 0, !.

subline_diag_up_left(B, Pos, N, List) :-
    Pos1 is Pos - N - 1,
    subline_diag_up_left_rec(B, Pos1, N, List).

subline_diag_up_left_rec(_, Pos, _, []) :-
    Pos =< 0, !.

subline_diag_up_left_rec(B, Pos, N, [(Pos, E)]) :-
   (Pos - 1) mod N =:= 0,
   element(B, Pos, E), !.

subline_diag_up_left_rec(B, Pos, N, [(Pos, E)|List]) :-
    element(B, Pos, E),
    Pos1 is Pos - N - 1,
    subline_diag_up_left_rec(B, Pos1, N, List).


% subline_diag_down_right(+Board, +Position, +N, -List)
%
%   Computes the sublist ( [(Position, Element)|_]) ) to the lower right
%   (diagonally) of Position.
%   Board has NxN positions.
%   Position starts at 1.
%
%   e.g. Position = 1, [a, b,
%                       c, d] -> [d]
%
subline_diag_down_right(_, Pos, N, []) :-
    Pos mod N =:= 0, !.

subline_diag_down_right(B, Pos, N, List) :-
    Pos1 is Pos + N + 1,
    subline_diag_down_right_rec(B, Pos1, N, List).

subline_diag_down_right_rec(_, Pos, N, []) :-
    Pos > N * N, !.

subline_diag_down_right_rec(B, Pos, N, [(Pos, E)]) :-
   Pos mod N =:= 0,
   element(B, Pos, E), !.

subline_diag_down_right_rec(B, Pos, N, [(Pos, E)|List]) :-
    element(B, Pos, E),
    Pos1 is Pos + N + 1,
    subline_diag_down_right_rec(B, Pos1, N, List).


% subline_diag_down_left(+Board, +Position, +N, -List)
%
%   Computes the sublist ( [(Position, Element)|_]) ) to the lower left
%   (diagonally) of Position.
%   Board has NxN positions.
%   Position starts at 1.
%
%   e.g. Position = 2, [a, b,
%                       c, d] -> [c]
%
subline_diag_down_left(_, Pos, N, []) :-
    (Pos - 1) mod N =:= 0, !.

subline_diag_down_left(B, Pos, N, List) :-
    Pos1 is Pos + N - 1,
    subline_diag_down_left_rec(B, Pos1, N, List).

subline_diag_down_left_rec(_, Pos, N, []) :-
    Pos > N * N, !.

subline_diag_down_left_rec(B, Pos, N, [(Pos, E)]) :-
   (Pos - 1) mod N =:= 0,
   element(B, Pos, E), !.

subline_diag_down_left_rec(B, Pos, N, [(Pos, E)|List]) :-
    element(B, Pos, E),
    Pos1 is Pos + N - 1,
    subline_diag_down_left_rec(B, Pos1, N, List).
