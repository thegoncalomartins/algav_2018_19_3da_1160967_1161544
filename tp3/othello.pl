:- module(othello, [moves/2, move/3, valid_move/5, next_player/2, min_to_move/1, max_to_move/1, utility/2, depth/1, alpha/1, beta/1]).

:- use_module(sublines).

depth(5).
alpha(-1000).
beta(1000).


moves(Pos, NextPosList) :-
    findall(NextPos, move(_, Pos, NextPos), NextPosList),
    NextPosList\==[].


move(Pos, [Player, play, Board], [NextPlayer, State, NextBoard]) :-
    next_player(Player, NextPlayer),
    valid_move(Board, Pos, Player, NextPlayer, PiecesToFlip),
    place_pieces(Board, [Pos|PiecesToFlip], Player, NextBoard),
    decide(Player, NextBoard, State).


% decide(+Player, +Board, -Result)
%
%    Decides if the game is over and if so, what the Result is for
%    Player.
%
decide(Player, Board, State) :-
    end_position(Board, W, B),
    decide_x(Player, W, B, State), !.

decide(_, _, play).

decide_x(w, W, B, win) :-
    W > B.

decide_x(b, W, B, win) :-
    B > W.

decide_x(w, W, B, loss) :-
    W < B.

decide_x(b, W, B, loss) :-
    B < W.

decide_x(_, _, _, draw).


% place_pieces(+Board, +Positions, +NewPiece, -NextBoard)
%
%   Replaces the pieces in Positions of the Board by NewPiece.
%
place_pieces(Board, [Pos|[]], Piece, FinalBoard) :-
    place_piece(Board, Pos, Piece, FinalBoard),
    !.

place_pieces(Board, [Pos|T], Piece, FinalBoard) :-
    place_piece(Board, Pos, Piece, NextBoard),
    place_pieces(NextBoard, T, Piece, FinalBoard).


place_piece([_|L], 1, E, [E|L]) :- !.

place_piece([H|T], P, E, [H|LR]) :-
    number(P),
    P1 is P - 1,
    place_piece(T, P1, E, LR).


% next_player(+P1, -P2)
%
%    P2 plays after P1
%
next_player(w, b).
next_player(b, w).


%  valid_move(+Board, +Pos, +Player, +Adversary, -PiecesToFlip)
%
%    Checks if Pos is a valid play for Player in Board and return the
%    position of the pieces to flip.
%
%    If a position is invalid, PiecesToFlip is an empty list.
%
valid_move(Board, Pos, Player, Adversary, Positions) :-
    empty_position(Board, Pos),
    sublines(Board, Pos, Sublines),
    valid_sublines(Player, Adversary, Sublines, Positions),
    Positions \== [].


% sublines(+Board, +Position, -SublineList)
%
%   Computes all the sublines of Position in Board.
%
sublines(Board, Pos, [N, NE, E, SE, S, SO, O, NO]) :-
    board_size(Rows),
    subline_above(Board, Pos, Rows, N),
    subline_diag_up_right(Board, Pos, Rows, NE),
    subline_right(Board, Pos, Rows, E),
    subline_diag_down_right(Board, Pos, Rows, SE),
    subline_below(Board, Pos, Rows, S),
    subline_diag_down_left(Board, Pos, Rows, SO),
    subline_left(Board, Pos, Rows, O),
    subline_diag_up_left(Board, Pos, Rows, NO).


%    eficiencia recursivade no fim
%
% valid_sublines(+Sublines, -ValidSublines)
%
%   Computes the ValidSublines from Sublines.
%   Only the valid part of the Subline is added to ValidSubLines.
%
valid_sublines(_, _, [], []).

valid_sublines(P, A,[SubL|T], L) :-
    validate_subline(P, A, SubL, Pos),
    valid_sublines(P, A, T, L1),
    append(L1, Pos, L), !.

valid_sublines(P, A, [_|T], L) :-
    valid_sublines(P, A, T, L).


validate_subline(Player, Adv, [(P, Adv)|T], [P|ValidSubL]) :-
    validate_subline_x(Player, Adv, T, ValidSubL),
    !.

validate_subline_x(Player, _, [(_, Player)|_], []).

validate_subline_x(Player, Adv, [(P, Adv)|T], [P|SubL]) :-
    validate_subline_x(Player, Adv, T, SubL).


% empty_position(+Board, ?Position)
%
%   Find an empty position (marked 0) on the board.
%
empty_position([0|_], 1).

empty_position([_|Board], Position) :-
    empty_position(Board, Position1),
    Position is Position1 + 1.


% end_position(+Board, -WhitePieces, -BlackPieces)
%
%   Checks if the game has reached an end position and computes the
%   number of pieces for each player.
%
end_position(Board, W, B) :-
    count_pieces(Board, W, B, 0), !.

end_position(Board, W, B) :-
    not(valid_move(Board, _, w, b, _)),
    not(valid_move(Board, _, b, w, _)),
    count_pieces(Board, W, B, _).


% count_pieces(+Board, -White, -Black, -Empty)
%
%   Counts the number of pieces and empty
%   spaces on the board.
%
count_pieces([], 0, 0, 0) :- !.

count_pieces([w|BoardT], White, Black, Empty) :-
  count_pieces(BoardT, White1, Black, Empty),
  White is White1 + 1, !.

count_pieces([b|BoardT], White, Black, Empty) :-
  count_pieces(BoardT, White, Black1, Empty),
  Black is Black1 + 1, !.

count_pieces([0|BoardT], White, Black, Empty) :-
  count_pieces(BoardT, White, Black, Empty1),
  Empty is Empty1 + 1, !.


% min_to_move(+Pos)
% True if the next player to play is the MIN player.
min_to_move([w, _, _]).

% max_to_move(+Pos)
% True if the next player to play is the MAX player.
max_to_move([b, _, _]).



utility([_, draw, _], 0).

utility([_, loss, Board], Val) :-
    count_pieces(Board, White, Black, _),
    Val is Black - White.

utility([_, win, Board], Val) :-
    count_pieces(Board, White, Black, _),
    Val is Black - White.

% https://courses.cs.washington.edu/courses/cse573/04au/Project/mini1/RUSSIA/Final_Paper.pdf

utility([_, play, Board], Val) :-
    coin_parity(Board, BV),
    corners_captured(Board, CV),
    mobility(Board, MV),
    F is 1 / 3,
    Val is (F * BV) + (F * CV) + (F * MV).

coin_parity(Board, Val) :-
    count_pieces(Board, White, Black, _),
    Val is 100 * (Black - White) / (Black + White).

corners_captured(Board, Val) :-
    board_size(N),
    index(N, 1, 1, C1),
    index(N, 1, N, C2),
    index(N, N, 1, C3),
    index(N, N, N, C4),
    element(Board, C1, E1),
    element(Board, C2, E2),
    element(Board, C3, E3),
    element(Board, C4, E4),
    corners_captured_x(Board, [E1, E2, E3, E4], B, W),
    corners_captured_val(B, W, Val).

corners_captured_x(_, [], 0, 0).

corners_captured_x(Board, [w|List], B, W) :-
  corners_captured_x(Board, List, B, W1),
  W is W1 + 1.

corners_captured_x(Board, [b|List], B, W) :-
  corners_captured_x(Board, List, B1, W),
  B is B1 + 1.

corners_captured_x(Board, [0|List], B, W) :-
  corners_captured_x(Board, List, B, W).

corners_captured_val(B, W, 0) :-
  (B + W) =:= 0.

corners_captured_val(B, W, Val) :-
  Val is 100 * (B - W) / (B + W).


mobility(Board, Val) :-
   findall(NP, move(_, [b, play, Board], NP), BNPL),
   length(BNPL, B),
   findall(NP, move(_, [w, play, Board], NP), WNPL),
   length(WNPL, W),
   Val is 100 * (B - W) / (B + W).
