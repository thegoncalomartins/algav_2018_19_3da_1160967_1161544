:- module(vs_human, [play_human/0]).

:- use_module(othello).

play_human :-
    nl,
    board_size(N),
    generate_initial_state_board(N, Board),
    show_board(N, Board), nl,
    play([b, Board], N).

play([Player, Board], N) :-
    next_player(Player, NextPlayer),
    not(valid_move(Board, _, Player, NextPlayer, _)),
    nl, write("No moves available for "), write(Player), nl,
    play([NextPlayer, Board], N).


play([Player, Board], N) :-
    next_player(Player, NextPlayer),
    nl, write(Player), write(' to move'),
    nl, write('Next move?'), nl,
    ask_coordinates_to_pos(Pos),
    move(Pos, [Player, play, Board], [NextPlayer, State, NextBoard]),
    show_board(N, NextBoard),
    play_x(Player, State, NextBoard, NextPlayer, N).

play([Player, Board], N) :-
    nl, write('Invalid move!'), nl,
    show_board(N, Board), nl,
    play([Player, Board], N).


% Game over: win
play_x(Player, win, _, _, _) :-
    nl, write('Game over'), nl,
    write(Player), write(' wins!'), nl, nl.

% Game over: loss
play_x(_, loss, _, NextPlayer, _) :-
    nl, write('Game over'), nl,
    write(NextPlayer), write(' wins!'), nl, nl.


% Game over: draw
play_x(_, draw, _, _, _) :-
    nl, write('End of game: '),
    write(' draw !'), nl, nl.

% Keep playing
play_x(_, play, NextBoard, NextPlayer, N) :-
    play([NextPlayer, NextBoard], N).