:- module(vs_minimax, [play_minimax/0]).

:- use_module(othello).
:- use_module(minimax).

play_minimax :-
    repeat,
      nl, write('Color? (b or w)'), nl,
      read(Player), nl,
      ( Player == b ; Player == w ),
      nl,
    board_size(N),
    generate_initial_state_board(N, Board),
    show_board(N, Board), nl,
    play_minimax([b, Board], Player, N).

play_minimax([Player, Board], Player, N) :-
    next_player(Player, NextPlayer),
    not(valid_move(Board, _, Player, NextPlayer, _)),
    nl, write("No moves available for "), write(Player), nl,
    play_minimax([NextPlayer, Board], Player, N).

play_minimax([Player, Board], Player, N) :-
    nl, write('Next move?'), nl,
    ask_coordinates_to_pos(Pos),
    move(Pos, [Player, play, Board], [NextPlayer, State, NextBoard]),
    show_board(N, NextBoard),
    play_minimax_x(Player, State, NextBoard, NextPlayer, Player, N).

play_minimax([Player, Board], HumanPlayer, N) :-
    Player \== HumanPlayer,
    nl, nl, write('Computer play:'), nl, nl,
    depth(D),
    minimax([Player, play, Board], [NextPlayer, State, NextBoard], _, D),
    show_board(N, NextBoard),
    play_minimax_x(Player, State, NextBoard, NextPlayer, HumanPlayer, N).


play_minimax([Player, Board], Player, N) :-
    nl, write('Invalid move!'), nl,
    show_board(N, Board), nl,
    play_minimax([Player, Board], Player, N).


% Game over: win
play_minimax_x(Player, win, _, _, _, _) :-
    nl, write('Game over'), nl,
    write(Player), write(' wins!'), nl, nl.

% Game over: loss
play_minimax_x(_, loss, _, NextPlayer, _, _) :-
    nl, write('Game over'), nl,
    write(NextPlayer), write(' wins!'), nl, nl.

% Game over: draw
play_minimax_x(_, draw, _, _, _, _) :-
    nl, write('End of game: '),
    write(' draw !'), nl, nl.

% Keep playing
play_minimax_x(_, play, NextBoard, NextPlayer, Player, N) :-
    play_minimax([NextPlayer, NextBoard], Player, N).
