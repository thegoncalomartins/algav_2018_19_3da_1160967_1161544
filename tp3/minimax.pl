:- module(minimax, [minimax/4]).

:- use_module(othello).

% minimax(Pos, BestNextPos, Val)
%
%   Pos is a position, Val is its minimax value.
%   Best move from Pos leads to position BestNextPos.
%
minimax(Pos, BestNextPos, Val, Depth) :-
    Depth1 is Depth - 1,
    Depth1 >= 0,
    moves(Pos, NextPosList),
    best(NextPosList, BestNextPos, Val, Depth1), !.

minimax([Player, play, Board], BestNextPos, Val, Depth) :-
    Depth1 is Depth - 1,
    Depth1 >= 0,
    next_player(Player, NextPlayer),
    best([[NextPlayer, play, Board]], BestNextPos, Val, Depth1).

minimax(Pos, _, Val, _) :-
    utility(Pos, Val).


best([Pos], Pos, Val, Depth) :-
    minimax(Pos, _, Val, Depth), !.

best([Pos1|PosList], BestPos, BestVal, Depth) :-
    minimax(Pos1, _, Val1, Depth),
    best(PosList, Pos2, Val2, Depth),
    better_of(Pos1, Val1, Pos2, Val2, BestPos, BestVal).


better_of(Pos0, Val0, _, Val1, Pos0, Val0) :-   % Pos0 better than Pos1
    min_to_move(Pos0),                          % MIN to move in Pos0
    Val0 > Val1, !.                               % MAX prefers the greater value

better_of(Pos0, Val0, _, Val1, Pos0, Val0) :-   % Pos0 better than Pos1
    max_to_move(Pos0),                          % MAX to move in Pos0
    Val0 < Val1, !.                             % MIN prefers the lesser value

better_of(_, _, Pos1, Val1, Pos1, Val1).        % Otherwise Pos1 better than Pos0
