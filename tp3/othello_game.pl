:- use_module(vs_human).
:- use_module(vs_minimax).
:- use_module(vs_alpha_beta).
:- use_module(board).

:-dynamic(board_size/1).


% Start the game.
play :-
    nl,
    write('===================='), nl,
    write('=     Othello    ='), nl,
    write('===================='), nl, nl,
    define_board_size,
    menu.

menu :-
    repeat,
      nl,
      write('1. Human vs Human.'), nl,
      write('2. Human vs Minimax.'), nl,
      write('3. Human vs Alpha-beta.'), nl,
      write('9. Edit board size.'), nl,
      write('0. Exit'), nl,
      nl,
      read(Option),
      number(Option),
      menu_x(Option).

menu_x(1) :-
    play_human,
    menu.

menu_x(2) :-
    play_minimax,
    menu.

menu_x(3) :-
    play_alpha_beta,
    menu.

menu_x(9) :-
    define_board_size,
    menu.

menu_x(0) :-
    retract(board_size(_)).


define_board_size :-
    ask_board_size(S),
    define_board_size_x(S).

define_board_size_x(S) :-
    retract(board_size(_)),
    assert(board_size(S)).

define_board_size_x(S) :-
    assert(board_size(S)).


% ask_coordinates_to_pos(-Pos)
%
%   Asks the user for the coordinates and converts them to a position.
%
ask_coordinates_to_pos(P) :-
    write('Hint: Write coordinates (Row,Col), e.g. 4,3. '), nl, nl,
    read((Row, Col)), nl,
    board_size(N),
    index(N, Row, Col, P).

% index(+TotalRows, +Row, +Col, -Index)
%
%   Based on TotalRows, Row and Col, returns the respective index on the
%   list.
%
index(TotalRows, Row, Col, Index) :-
    Index is (Row - 1) * TotalRows + Col.
