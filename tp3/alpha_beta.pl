:- module(alpha_beta, [alpha_beta/6]).

:- use_module(othello).

alpha_beta(Pos, Alpha, Beta, GoodPos, Val, Depth) :-
  Depth1 is Depth - 1,
  Depth1 >= 0,
  moves(Pos, PosList),
  bounded_best(PosList, Alpha, Beta, GoodPos, Val, Depth1).

alpha_beta([Player, play, Board], Alpha, Beta, GoodPos, Val, Depth) :-
  Depth1 is Depth - 1,
  Depth1 >= -1000,
  next_player(Player, NextPlayer),
  bounded_best([[NextPlayer, play, Board]], Alpha, Beta, GoodPos, Val, Depth1),
  !.

alpha_beta(Pos, _, _, _, Val, _) :-
  utility(Pos, Val).


bounded_best([Pos|PosList], Alpha, Beta, GoodPos, GoodVal, Depth) :-
  alpha_beta(Pos, Alpha, Beta, _, Val, Depth),
  good_enough(PosList, Alpha, Beta, Pos, Val, GoodPos, GoodVal, Depth).


good_enough([], _, _, Pos, Val, Pos, Val, _) :- !.

good_enough(_, _, Beta, Pos, Val, Pos, Val, _) :-
  min_to_move(Pos),
  Val > Beta,
  !.

good_enough(_, _, Alpha, Pos, Val, Pos, Val, _) :-
  max_to_move(Pos),
  Val < Alpha,
  !.

good_enough(PosList, Alpha, Beta, Pos, Val, GoodPos, GoodVal, Depth) :-
  new_bounds(Alpha, Beta, Pos, Val, NewAlpha, NewBeta),
  bounded_best(PosList, NewAlpha, NewBeta, Pos1, Val1, Depth),
  better_of_ab(Pos, Val, Pos1, Val1, GoodPos, GoodVal).


new_bounds(Alpha, Beta, Pos, Val, Val, Beta) :-
  min_to_move(Pos),
  Val > Alpha,
  !.

new_bounds(Alpha, Beta, Pos, Val, Alpha, Val) :-
  max_to_move(Pos),
  Val < Beta,
  !.

new_bounds(Alpha, Beta, _, _, Alpha, Beta).


better_of_ab(Pos, Val, _, Val1, Pos, Val) :-
  min_to_move(Pos),
  Val > Val1,
  !.

better_of_ab(Pos, Val, _, Val1, Pos, Val) :-
  max_to_move(Pos),
  Val < Val1,
  !.

better_of_ab(_, _, Pos1, Val1, Pos1, Val1).
