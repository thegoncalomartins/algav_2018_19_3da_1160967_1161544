:- module(board, [generate_initial_state_board/2, ask_board_size/1, show_board/2]).

% generate_initial_state_board(+BoardSize, -Board)
%
%   Generates the initial state Board with given size BoardSize.
%
generate_initial_state_board(N, Board) :-
  generate_inital_state_board_x(N, 1, Board),
  !.

generate_inital_state_board_x(N, C, [0]) :-
  C =:= N * N.

generate_inital_state_board_x(N, C, [w|Board]) :-
  N2 is N / 2,
  C =:= ((N2 - 1) * N) + N2,
  C1 is C + 1,
  generate_inital_state_board_x(N, C1, Board).

generate_inital_state_board_x(N, C, [b|Board]) :-
  N2 is N / 2,
  C =:= ((N2 - 1) * N) + N2 + 1,
  C1 is C + 1,
  generate_inital_state_board_x(N, C1, Board).

generate_inital_state_board_x(N, C, [b|Board]) :-
  N2 is N / 2,
  C =:= (N2 * N) + N2,
  C1 is C + 1,
  generate_inital_state_board_x(N, C1, Board).

generate_inital_state_board_x(N, C, [w|Board]) :-
  N2 is N / 2,
  C =:= (N2 * N) + N2 + 1,
  C1 is C + 1,
  generate_inital_state_board_x(N, C1, Board).

generate_inital_state_board_x(N, C, [0|Board]) :-
  C1 is C + 1,
  generate_inital_state_board_x(N, C1, Board).





% ask_board_size(-BoardSize)
%
%   Asks the user to input the size of the board and validates it.
%
%   Board size has to be:
%     - an even number;
%     - more or equal to 4;
%
ask_board_size(S) :-
  repeat,
  write('Board size (even number >= 4):'),
  nl,
  read(S),
  validate_board_size(S).

validate_board_size(S) :-
  not(number(S)),
  nl, write_error_message(S, 'is not a number.'), nl,
  !,
  false.

validate_board_size(S) :-
  S < 4,
  nl, write_error_message(S, 'is less than 4.'), nl,
  !,
  false.

validate_board_size(S) :-
  S mod 2 =\= 0,
  nl, write_error_message(S, 'is not an even number.'), nl,
  !,
  false.

validate_board_size(_).


% write_error_message(+Data, +Message)
%
%. Outputs an error message in the format
%  "error: Data Message"
%
write_error_message(D, M) :-
  write('error: '),
  write(D), write(' '),
  write(M), nl.


show_board(N, B) :-
  write_column_numbers(N, 1),
  write_line(B, N, 1),
  !.

write_column_numbers(N, N) :-
  write('  '), write(N), nl.

write_column_numbers(N, 1) :-
  write('   1 '),
  write_column_numbers(N, 2).

write_column_numbers(N, C) :-
  write('  '), write(C), write(' '),
  C1 is C + 1,
  write_column_numbers(N, C1).

write_line(L, N, N) :-
  write(N), write('  '),
  write_elems(L, N, 1, _).

write_line(L, N, Row) :-
  write(Row), write('  '),
  write_elems(L, N, 1, NL),
  N1 is N * 4,
  write('  '), write_line_separator(-, N1, 1), nl,
  Row1 is Row + 1,
  write_line(NL, N, Row1).

write_elems([E|Board], N, N, Board) :-
  write_element_or_empty(E), nl.

write_elems([E|Board], N, C, RB) :-
  write_element_or_empty(E), write(' | '),
  C1 is C + 1,
  write_elems(Board, N, C1, RB).

write_element_or_empty(E) :-
    E = 0, !,
    write(' ').

write_element_or_empty(E) :-
    write(E).

write_line_separator(S, N, N) :-
  write(S).

write_line_separator(S, N, C) :-
  write(S),
  C1 is C + 1,
  write_line_separator(S, N, C1).

