:- consult('bicicletas.txt').
% Predicados auxiliares
appendl([], L, L).
appendl([X|L1], L2, [X|L3]) :- appendl(L1, L2, L3).

% Predicados

% 2.
gerar_elemento :-
    findall(X, (componente(X, _, _);componente(_, X, _)), L),
    sort(L, NL),
    gerar_elementos(NL).

gerar_elementos([]).

gerar_elementos([H|T]) :-
    assert(elemento(H)),
    gerar_elementos(T).

% 3.
produto_final(Elemento) :-
    elemento(Elemento),
    not(componente(_, Elemento, _)).

% 4.
produto_base(Elemento) :-
    elemento(Elemento),
    not(componente(Elemento, _, _)).

% 5.
produto_intermedio(Elemento) :-
    elemento(Elemento),
    not(produto_final(Elemento)),
    not(produto_base(Elemento)).

% 6.
anc(Elem, Elem, 0) :-
    not(componente(_, Elem, _)).

anc(Elem, A, 1) :-
    componente(A, Elem, _).

anc(Elem, A, Nvl) :-
    componente(Pai, Elem, _),
    anc(Pai, A, NvlTemp),
    Nvl is NvlTemp + 1.

nivel(ElemX, ElemY, Nvl) :-
    anc(ElemX, A, Nvl1),
    anc(ElemY, A, Nvl2),
    Nvl is abs(Nvl1 - Nvl2),
    !.

% 7.
descendente(Elem, D, 1) :-
    componente(Elem, D, _).

descendente(Elem, D, Profundidade) :-
    componente(Elem, Filho, _),
    descendente(Filho, D, Profundidade1),
    Profundidade is Profundidade1 + 1.

mais_profundo(Raiz, (X, PX)) :-
    descendente(Raiz, X, PX),
    not((
        descendente(Raiz, _, PY),
        PY > PX
    )).

dois_mais_profundos(ElementoRaiz,(EMax1,Prof1),(EMax2,Prof2)) :-
                    mais_profundo(ElementoRaiz, (EMax1, Prof1)),
                    mais_profundo(ElementoRaiz, (EMax2, Prof2)),
                    EMax1\==EMax2.


% 8.
reg_custo(Elemento, Custo) :-
    retract(custo(Elemento, _)),
    assert(custo(Elemento, Custo)), !.

reg_custo(Elemento, Custo) :-
    assert(custo(Elemento, Custo)).

% 9.
calc_custo(Elemento, Valor, _) :-
    custo(Elemento, Valor), !.

calc_custo(Elemento, Valor, LElemSemCusto) :-
    findall(V1, (
                componente(Elemento, X, QT),
                calc_custo(X, C, _),
                V1 is C * QT
            ), L),
    soma_lista(L, Valor),
    findall(X, (
                descendente(Elemento, X, _),
                not(custo(X, _))
            ), LElemSemCusto).

soma_lista([H], H).

soma_lista([H|T], Sum) :-
    soma_lista(T, Sum1),
    Sum is Sum1 + H, !.

% 10.
descendente_quantidade(Elem, D, Qtd, C) :-
    componente(Elem, D, Qtd), custo(D, C).

descendente_quantidade(Elem, D, Qtd, C) :-
    componente(Elem, F, Qtd1),
    descendente_quantidade(F, D, Qtd2, C),
    Qtd is Qtd1 * Qtd2.

lista_materiais(Elemento, Quantidade) :-
    findall((X,Qtd, C), (
                descendente_quantidade(Elemento, X, Qtd, C),
                produto_base(X)
            ), L),
    findall((C2, R1, X), (
                member((X, _, C), L),
                quantidade(X, L, R),
                C1 is C * R,
                C2 is C1 * Quantidade,
                R1 is R * Quantidade
            ), Qtds),
    sort(0, @>, Qtds, NQtds),
    write(Elemento),
    write(" : "),
    write(Quantidade),
    nl,
    write("------------------------------------"),
    nl,
    escrever_lista(NQtds).

quantidade(_, [], 0).

quantidade(X, [(X,Qtd,_)|T], R) :-
    quantidade(X, T, R1),
    R is R1 + Qtd,
    !.

quantidade(X, [_|T], R) :-
    quantidade(X, T, R).

escrever_lista([]).

escrever_lista([(C, Qtd, Elem)|T]) :-
    write(Elem),
    write(","),
    write(Qtd),
    write(","),
    write(C),
    nl,
    escrever_lista(T).

% 11.
produto_arvore(Elemento, Elemento) :-
    produto_base(Elemento), !.

produto_arvore(Elemento, (Elemento, LC)) :-
    findall(L, (componente(Elemento, X, _),
    produto_arvore(X, L)), LC).

% 12.
descendente_qtd_sem_custo(Elem, D, Qtd) :-
    componente(Elem, D, Qtd).

descendente_qtd_sem_custo(Elem, D, Qtd) :-
    componente(Elem, F, Qtd1),
    descendente_qtd_sem_custo(F, D, Qtd2),
    Qtd is Qtd1 * Qtd2.


listar_arvore_identada(Elemento, off, off, _) :-
    write(Elemento), !.

listar_arvore_identada(Elemento, off, on, off) :-
    !,
    write(Elemento),
    nl,
    descendente(Elemento, X, _),
    produto_base(X),
    tab(4),
    write(X),
    nl,
    fail.

listar_arvore_identada(Elemento, off, on, on) :-
    !,
    write(Elemento),
    nl,
    descendente_qtd_sem_custo(Elemento, X, Qtd),
    produto_base(X),
    tab(4),
    write((X,Qtd)),
    nl,
    fail.

listar_arvore_identada(Elemento, on, off, on) :-
    !,
    write(Elemento),
    nl,
    descendente_qtd_sem_custo(Elemento, X, Qtd),
    produto_intermedio(X),
    tab(4),
    write((X,Qtd)),
    nl,
    fail.

listar_arvore_identada(Elemento, on, off, off) :-
    !,
    write(Elemento),
    nl,
    descendente_qtd_sem_custo(Elemento, X, _),
    produto_intermedio(X),
    tab(4),
    write(X),
    nl,
    fail.


listar_arvore_identada(Elemento, on, on, off) :-
    write(Elemento),
    nl,
    componente(Elemento, Y, _),
    listar_ident(Y, 4),
    fail.

listar_arvore_identada(Elemento, on, on, on) :-
    write(Elemento),
    nl,
    componente(Elemento, Y, Qtd),
    listar_ident_qtd(Y, 4, Qtd),
    fail.

listar_ident(X, IDENT) :-
    tab(IDENT),
    write(X),
    nl,
    componente(X, Y, _),
    IDENT1 is IDENT + 4,
    listar_ident(Y, IDENT1).


listar_ident_qtd(X, IDENT, Qtd) :-
    tab(IDENT),
    write((X, Qtd)),
    nl,
    componente(X, Y, Qtd1),
    IDENT1 is IDENT + 4,
    listar_ident_qtd(Y, IDENT1, Qtd1).

% 13.
/*
guardarBaseConhecimento(Nome) :-
    tell(Nome),
    listing(componente),
    listing(elemento),
    listing(custo),
    told.
*/

% 13.
/*
guardarBaseConhecimento(Nome) :-
    tell(Nome),
    listing,
    told.
*/
