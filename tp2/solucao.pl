:- consult('cidades.txt').
:- consult('reordenar.txt').
:- consult('detetar_intersecoes.txt').

%delete(X, [X|T], T).
%delete(X, [H|T], [H|L]) :- delete(X, T, L).

append([], L, L).
append([X|L1], L2, [X|L3]) :- append(L1, L2, L3).

member(X,[X|_]).
member(X,[_|T]):- member(X,T).

/* ############## ITERACAO 1 ############## */
% Algoritmo Branch and Bound

count_cities(N) :-
  findall(C, city(C, _, _), L),
  length(L, N).

tsp1(Orig, Cam, Custo) :-
  count_cities(NCidades),
  bnb([(0, [Orig])], Cam, Custo, NCidades, Orig).
% Tem capacidade para resolver 9 cidades, em alguns segundos.

bnb([(Custo,[Orig|Cam])|_],CamFinal,Custo, _, Orig) :-
  Cam\==[],
  reverse([Orig|Cam], CamFinal).

bnb([(Ca,LA)|Outros], Cam, Custo, NCidades, Orig) :-
  length(LA, NCidades),
  append([Orig], LA, NLA),
  LA=[Act|_],
  dist_cities(Act, Orig, N),
  NCa is Ca + N,
  append(Outros,[(NCa, NLA)],Todos),
  sort(Todos,TodosOrd),
  !,
  bnb(TodosOrd,Cam,Custo, NCidades, Orig).

bnb([(Ca,LA)|Outros], Cam, Custo, NCidades, Orig) :-
   LA=[Act|_],
  findall((CaX,[City|LA]),(city(City, _, _), \+member(City,LA), dist_cities(Act, City, Dist), CaX is Dist + Ca ),Novos),
  append(Outros,Novos,Todos),
  sort(Todos,TodosOrd),
  bnb(TodosOrd,Cam,Custo, NCidades, Orig).




/* ############## ITERACAO 2 ############## */
% Algoritmo Best First Search (BestFS)
% falta adapta��o
tsp2(Orig, Cam, Custo) :-
  count_cities(NCities),
  bestfs(NCities, [Orig], Cam, 0, Custo).

bestfs(1, L, Cam, CustoSemOrig, CustoFinal) :- !,
  L=[Last|_],
  reverse(L, Cam1),
  Cam1=[Orig|_],
  append(Cam1, [Orig], Cam),
  dist_cities(Orig, Last, C),
  CustoFinal is CustoSemOrig + C.


bestfs(NCities, LA, Cam, Custo, CustoFinal) :-
  LA=[Act|_],
  findall((EstX,[X|LA]),
         (city(X, _, _),
          not(member(X, LA)),
          dist_cities(Act, X, EstX)), Novos),
  sort(Novos, NovosOrd),
  NovosOrd = [(C, Melhor)|_],
  Custo1 is Custo + C,
  NCities1 is NCities - 1,
  bestfs(NCities1, Melhor, Cam, Custo1, CustoFinal).




% ################### ITERACAO 3 ############################
% toSegment(L, NewL).
% [a,b,c] => [[a,b], [b,c]]
toSegment([H|[H1|[]]], [[H, H1]]).

toSegment([H|[H1|T]], NNL) :-
    toSegment([H1|T], NewL),
    append([[H, H1]], NewL, NNL), !.


intersect(P1, Q1, P2, Q2) :-
  city(P1, P1X, P1Y),
  city(Q1, Q1X, Q1Y),
  city(P2, P2X, P2Y),
  city(Q2, Q2X, Q2Y),
  doIntersect((P1X, P1Y), (Q1X, Q1Y), (P2X, P2Y), (Q2X, Q2Y)).


opt2_append(Orig, Segmentos, P1, Q1, P2, Q2, SegmentosFinal) :-
  append(Segmentos, [[P1, P2]], Segmentos1),
  append(Segmentos1, [[Q1, Q2]], Segmentos2),
  rGraph(Orig, Segmentos2, SegmentosFinal).
opt2_append(Orig, Segmentos, P1, Q1, P2, Q2, SegmentosFinal) :-
  append(Segmentos, [[P1, Q2]], Segmentos1),
  append(Segmentos1, [[Q1, P2]], Segmentos2),
  rGraph(Orig, Segmentos2, SegmentosFinal).


opt2_del(Orig, Segmentos, P1, Q1, P2, Q2, SegmentosFinal) :-
  delete(Segmentos, [P1, Q1], Segmentos1),
  delete(Segmentos1, [P2, Q2], Segmentos2),
  opt2_append(Orig, Segmentos2, P1, Q1, P2, Q2, SegmentosFinal).


opt2(Orig, Segmentos, Solucao) :-
  member([P1, Q1], Segmentos),
  member([P2, Q2], Segmentos),
  [P1, Q1] \== [P2, Q2],
  Q1 \==  P2,
  P1 \== Q2,
  intersect(P1, Q1, P2, Q2),
  opt2_del(Orig, Segmentos, P1, Q1, P2, Q2, Segmentos1),
  opt2(Orig, Segmentos1, Solucao).

opt2(_, Segmentos, Segmentos).


custo([[H,H1]|[]], Custo) :-
  dist_cities(H, H1, Custo).

custo([[H,H1]|T], CustoFinal) :-
  dist_cities(H, H1, Custo1),
  custo(T, Custo),
  CustoFinal is Custo + Custo1.


tsp3(Orig, CamFinal, Custo) :-
  tsp2(Orig, Cam, _),
  toSegment(Cam, Segmentos),
  opt2(Orig, Segmentos, CamFinal),
  custo(CamFinal, Custo),
  !.



% ################### ITERACAO 4 ############################

population(100).
generations(30).
crossing_prob(0.8).
mutation_prob(0.8).

tsp4(Orig, CamFinal, Custo) :-
  gerar_pop(Orig, Pop),
  avaliar_pop(Orig, Pop, PopEv),
  sort(PopEv, PopEvSorted),
  generations(N),
  generate(Orig, N, PopEvSorted, Best),
  Best=[(Custo, Cam1)|_],
  append([Orig], Cam1, Cam2),
  append(Cam2, [Orig], CamFinal),
  !.

% gerar_pop(+Origem, -Populacao)
%
%   Gera uma populacao com population(N) individuos aleatorios, cada um
%   com inicio e fim em Origem.
%
%   e.g. population(2), Origem = brussels, Populacao = [[brussels,
%   minsk, tirana, andorra, brussels], [brussels, tirana, minsk,
%   andorra, brussels]]
%
gerar_pop(Orig, Pop) :-
  population(PopSize),
  findall(City, (city(City, _, _), City\==Orig), CityList),
  gerar_pop_x(PopSize, CityList, Pop, Orig).

gerar_pop_x(0, _, [], _) :- !.

gerar_pop_x(PopSize, CityList, [Ind|Others], Orig) :-
  PopSize1 is PopSize - 1,
  gerar_pop_x(PopSize1, CityList, Others, Orig),
  (   random_permutation(CityList, Ind) ; Ind=CityList),
  \+ member(Ind, Others).


% avaliar_pop(+Orig, +Populacao, -PopulacaoAvaliada)
%
%   Avalia os individuos de uma populacao tendo em conta Origem como
%   ponto de partida e destino ([Origem, Caminho, Origem]). Neste caso
%   um indivio é um caminho e a sua avaliacao é a distancia para
%   percorrer o caminho.
%
%   A PopulacaoAvaliada está no formato
%   [(Avaliacao, Individuo)|RestoDaPopulacaoAvaliada)].
%
avaliar_pop(_, [], []) :- !.

avaliar_pop(Orig, [I|Pop], [(V, I)|PopV]) :-
  append([Orig], I, I1),
  append(I1, [Orig], I2),
  distancia(I2, V),
  avaliar_pop(Orig, Pop, PopV).


distancia([C1,C2|[]], Dist) :-
  dist_cities(C1, C2, Dist),
  !.

distancia([C1,C2|T], Dist) :-
  dist_cities(C1, C2, Dist1),
  distancia([C2|T], Dist2),
  Dist is Dist1 + Dist2.

/*
generate(_, 0,Pop, Pop):-!,
	write('Generation '), write(0), write(':'), nl, write(Pop), nl.


generate(Orig, G,Pop, Best):-
	write('Generation '), write(G), write(':'), nl, write(Pop), nl,
	cruzamento(Pop,NPop1),
	mutacao(NPop1,NPop),
        avaliar_pop(Orig, NPop,NPopAv),
	sort(NPopAv,NPopOrd),
	G1 is G-1,
	generate(Orig, G1,NPopOrd, Best).
*/

generate(_, 0, Best, Best):-!.



generate(Orig, G, Pop, Best):-
	cruzamento(Pop,NPop1),
	mutacao(NPop1,NPop),
        avaliar_pop(Orig, NPop,NPopAv),
	sort(NPopAv,NPopOrd),
	G1 is G-1,
	generate(Orig, G1,NPopOrd, Best).


cruzamento([], []).

cruzamento([(_, I)], [I]).

cruzamento([(_, I1), (_, I2)|Other], [NI1, NI2|Other1]) :-
  gerar_pontos_cruzamento(P1, P2),
  crossing_prob(CProb),
  random(0.0, 1.0, P),
  cruzamento_prob(P, CProb, I1, I2, P1, P2, NI1, NI2),
  cruzamento(Other, Other1).


gerar_pontos_cruzamento(P1, P2) :-
  count_cities(N),
  random(1, N, R1),
  random(1, N, R2),
  R1\==R2,
  ( (R1 < R2, !, P1=R1, P2=R2) ; (P1=R2, P2=R1)).

gerar_pontos_cruzamento(P1, P2) :-
  gerar_pontos_cruzamento(P1, P2).


cruzamento_prob(P, CProb, I1, I2, P1, P2, NI1, NI2) :-
  P =< CProb,
  !,
  cruzar(I1, I2, P1, P2, NI1),
  cruzar(I2, I1, P1, P2, NI2).

cruzamento_prob(_, _, I1, I2, _, _, I1, I2).


cruzar(Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),
	count_cities(N),
        N1 is N - 1,
	R is N1-P2,
	rotate_right(Ind2,R,Ind21),
	elimina(Ind21,Sub1,Sub2),
	P3 is P2 + 1,
	insere(Sub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).

sublista(L1,I1,I2,L):-
	I1 < I2,
        !,
	sublista_x(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista_x(L1,I2,I1,L).

sublista_x([X|R1],1,1,[X|H]):-
        !,
	preencheh(R1,H).

sublista_x([X|R1],1,N2,[X|R2]):-
        !,
	N3 is N2 - 1,
	sublista_x(R1,1,N3,R2).

sublista_x([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista_x(R1,N3,N4,R2).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


rotate_right(L,K,L1):-
        count_cities(N),
        N1 is N - 1,
	T is N1 - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).


insere([],L,_,L):-!.

insere([X|R],L,N,L2):-
	count_cities(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere_x(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).

insere_x(X,1,L,[X|L]):-!.

insere_x(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere_x(X,N1,L,L1).


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).


mutacao([],[]).

mutacao([Ind|Rest],[NInd|Rest1]):-
	mutation_prob(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).
