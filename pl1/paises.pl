continente(europa).
continente(asia).
continente(americanorte).
continente(antartida).
continente(africa).
continente(americasul).
continente(oceania).
continente(africa).
pais(portugal, europa, 10).
pais(espanha, europa, 47).
pais(franca, europa, 60).
pais(mexico, americanorte, 120).
fronteira(portugal, espanha).
fronteira(espanha, franca).
fronteira(franca, suica).

vizinho(P1, P2):-fronteira(P1, P2).
vizinho(P1, P2):-fronteira(P2, P1).

contSemPaises(C) :- continente(C),\+pais(_, C, _).

semVizinhos(L) :- pais(L, _, _), \+vizinho(L, _).

chegoLaFacil(P1, P2) :- vizinho(P1, P2).
chegoLaFacil(P1, P2) :- vizinho(P1, P3), vizinho(P2, P3).
