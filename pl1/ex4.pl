potencia(_, 0, 1) :- !.
potencia(B, E, R) :- E < 0, E1 is -E, potencia(B, E1, R1), R is 1 / R1.
potencia(B, E, R) :- E1 is E-1, potencia(B, E1, R1), R is B * R1.

factorial(0,1) :- !.
factorial(N, F) :- N1 is N-1, factorial(N1, F1), F is N * F1.

somatorio(J, J, J) :- !.
somatorio(J, K, S) :- J =< K, J1 is J+1, somatorio(J1, K, S1), S is J + S1.

div_inteira(_, 0, _, _) :- !, fail.
div_inteira(DIVIDENDO, DIVISOR, 0, DIVIDENDO) :- DIVIDENDO < DIVISOR, !.
div_inteira(DIVIDENDO, DIVISOR, QUOCIENTE, RESTO) :- DIVIDENDO1 is DIVIDENDO-DIVISOR, div_inteira(DIVIDENDO1, DIVISOR, QUOCIENTE1, RESTO), QUOCIENTE is QUOCIENTE1 + 1.

primo(P) :- P>=2, primo_rec(P, 2).
primo_rec(P, P) :- !.
primo_rec(P, DIV) :- (P mod DIV) =\= 0, DIV1 is DIV+1, primo_rec(P, DIV1).

mdc(A, B, R) :- B > A, !,mdcr(B, A, A, R).
mdc(A, B, R) :- mdcr(A, B, B, R).
mdcr(A, B, TEST, TEST) :- (A mod TEST) =:= 0, (B mod TEST) =:= 0, !.
mdcr(A, B, TEST, R) :- TEST1 is TEST-1, mdcr(A, B, TEST1, R).
