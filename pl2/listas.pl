% a)
contar([], 0).
contar([_|T], N) :- contar(T, N1), N is N1 + 1.
% number(H) para ver se é número
somar([], 0).
somar([H|T], S) :- number(H), somar(T, S1), S is S1 + H.
media([], _) :- write("lista vazia"), !, fail.
media(Lista, M) :- somar(Lista, S), contar(Lista, C), M is S / C.

% b)
% calcular primeiro o menor da cauda e comparar com o head
% o menor é quando houver só um elemento:  menor([H], H).
menor([], _) :- write("lista vazia"), !, fail.
menor([H], H) :- !.
menor([H|T], M) :- menor(T, M1), H >= M1, M is M1, !.
menor([H|_], H).

% c)
pares([], 0, 0).
pares([H|T], P, I) :- pares(T, P1, I), (H mod 2) =:= 0, P is P1 + 1, !.
pares([_|T], P, I) :- pares(T, P1, I1), I is I1 + 1, P is P1.

% d)
% member da tp
member(X, [X|_]).
member(X, [_|T]) :- member(X, T).
% ver se a head está na tail
% se não estiver ver se a head da tail está na tail
repetidos([H|T]) :- member(H, T), !.
repetidos([_|[H1|T]]) :- member(H1, T).

% e)
% delete da aula tp
delete(X, [X|T], T).
delete(X, [H|T], [H|L]) :- delete(X, T, L).
menor_frente(L, [M|LN]) :- menor(L, M), delete(M, L, LN), !.

% f)
% append da aula tp
append([], L, L).
append([X|L1], L2, [X|L3]) :- append(L1, L2, L3).

% g)
% flatten(L1, L2)
flatten([], []).
flatten([[H|T]|TR], LR) :- !, flatten([H|T], L), flatten(TR, FTR), append(L, FTR, LR).
flatten([H|T], [H|LR]) :- flatten(T, LR).


% i)
delete_all(_, [], []) :- !.
delete_all(X, [X|T], LR) :- delete_all(X, T, LR), !.
delete_all(X, [H|T], [H|LR]) :- delete_all(X, T, LR).

% j)
%replace(X1, X2, L, LR)
replace(_, _, [], []) :- !.
replace(X1, X2, [X1|T], [X2|LR]) :- replace(X1, X2, T, LR), !.
replace(X1, X2, [H|T], [H|LR]) :- replace(X1, X2, T, LR).

% k)
% insert(X, N, L, LR)
insert(X, 0, T, [X|T]) :- !.
insert(X, N, [H|T], [H|LR]) :- N1 is N - 1, insert(X, N1, T, LR).

% l)
% reverse da tp
reverse(L, RL) :- accRev(L, [], RL).
accRev([], R, R).
accRev([H|T], A, R) :- accRev(T, [H|A], R).

% m)
%uniao(L1, L2, L3)
uniao([], L2, L2).
uniao([H|T], L2, [H|L3]) :- not(member(H, L2)), uniao(T, L2, L3), !.
uniao([_|T], L2, L3) :- uniao(T, L2, L3).

% n)
% intersec(L1, L2, L3)
intersec([], _, []).
intersec([H|T], L2, [H|L3]) :- member(H, L2), delete(H, L2, NL2), intersec(T, NL2, L3), !.
intersec([_|T], L2, L3) :- intersec(T, L2, L3).

% o)
% dif(L1, L2, L3)
% assumindo que não há elementos repetidos nas listas
dif([], L2, L2).
dif([H|T], L2, [H|L3]) :- not(member(H, L2)), dif(T, L2, L3), !.
dif([H|T], L2, L3) :- delete(H, L2, NL2), dif(T, NL2, L3).

